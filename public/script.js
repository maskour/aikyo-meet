const socket = io("/");
const videoGrids = document.getElementById("video-grids");
const myVideo = document.createElement("video");
const showChat = document.querySelector("#showChat");
const backBtn = document.querySelector(".header__back");
otherUser = "";
myVideo.muted = true;

backBtn.addEventListener("click", () => {
  document.querySelector(".main__left").style.display = "flex";
  document.querySelector(".main__left").style.flex = "1";
  document.querySelector(".main__right").style.display = "none";
  document.querySelector(".header__back").style.display = "none";
});

showChat.addEventListener("click", () => {
  document.querySelector(".main__right").style.display = "flex";
  document.querySelector(".main__right").style.flex = "1";
  document.querySelector(".main__left").style.display = "none";
  document.querySelector(".header__back").style.display = "block";
});


const peer = new Peer(undefined, {
  path: "/peerjs",
  host: "/",
  port: "3000",
});

let myVideoStream;
const peers = {};
const getUserMedia =
    navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia;

navigator.mediaDevices
  .getUserMedia({
    audio: true,
    video: true,
  })
  .then((stream) => {
    myVideoStream = stream;
    addVideoStream(myVideo, stream, USER_NAME);

    socket.on("user-connected", (userId, userName) => {
      connectToNewUser(userId, stream, userName);
      socket.emit("give-name", USER_NAME);
    });
    socket.on('user-disconnected', (userId) => {
        if (peers[userId]) peers[userId].close();
    });
  });

peer.on("call", (call) => {
  getUserMedia({ video: true, audio: true },
      function(stream) {
        call.answer(stream); // Answer the call with an A/V stream.
        const video = document.createElement("video");
        call.on("stream", function(remoteStream) {
          addVideoStream(video, remoteStream, otherUser);
        });
      },
      function(err) {
        console.log("Failed to get local stream", err);
      }
  );
});
peer.on("open", (id) => {
  socket.emit("join-room", ROOM_ID, id, USER_NAME);
});

socket.on('giveName', (name) => {
  const toastr = document.getElementById('toastr');
  toastr.innerHTML = `${name} joined the meeting`;
  toastr.classList.add('message');
  otherUser = name;
})

const RemoveUnusedDivs = () => {
  alldivs = videoGrids.getElementsByTagName("div");
  const len = alldivs.length;
  for (let i = 0; i < len; i++) {
    e = alldivs[i].getElementsByTagName("video").length;
    if (e === 0) {
      alldivs[i].remove();
    }
  }
};

const connectToNewUser = (userId, stream, name) => {
  const call = peer.call(userId, stream);
  const video = document.createElement("video");
  call.on("stream", (userVideoStream) => {
    addVideoStream(video, userVideoStream, name);
  });
  call.on("close", () => {
    video.remove();
    // RemoveUnusedDivs();
  });
  peers[userId] = call;
};

const addVideoStream = (video, stream, name) => {
  video.srcObject = stream;
  video.addEventListener("loadedmetadata", () => {
    video.play();
  });
  const h1 = document.createElement("h1");
  const h1name = document.createTextNode(name);
  h1.appendChild(h1name);
  h1.style = "color: white; font-size: 20px; margin: 0; padding: 0; text-align: center;";
  const videoGrid = document.createElement("div");
  videoGrid.classList.add("video-grid");
  videoGrid.appendChild(h1);
  videoGrids.appendChild(videoGrid);
  videoGrid.append(video);
  // RemoveUnusedDivs();
  // let totalUsers = document.getElementsByTagName("video").length;
  // if (totalUsers > 1) {
  //   for (let index = 0; index < totalUsers; index++) {
  //     document.getElementsByTagName("video")[index].style.width =
  //         100 / totalUsers + "%";
  //   }
  // }
};

let text = document.querySelector("#chat_message");
let send = document.getElementById("send");
let messages = document.querySelector(".messages");

send.addEventListener("click", (e) => {
  if (text.value.length !== 0) {
    socket.emit("send-message", text.value);
    text.value = "";
  }
});

text.addEventListener("keydown", (e) => {
  if (e.key === "Enter" && text.value.length !== 0) {
    socket.emit("send-message", text.value);
    text.value = "";
  }
});

const inviteButton = document.querySelector("#inviteButton");
const muteButton = document.querySelector("#muteButton");
const stopVideo = document.querySelector("#stopVideo");
muteButton.addEventListener("click", () => {
  const enabled = myVideoStream.getAudioTracks()[0].enabled;
  if (enabled) {
    myVideoStream.getAudioTracks()[0].enabled = false;
    html = `<i class="fas fa-microphone-slash"></i>`;
    muteButton.classList.toggle("background__red");
    muteButton.innerHTML = html;
  } else {
    myVideoStream.getAudioTracks()[0].enabled = true;
    html = `<i class="fas fa-microphone"></i>`;
    muteButton.classList.toggle("background__red");
    muteButton.innerHTML = html;
  }
});

stopVideo.addEventListener("click", () => {
  const enabled = myVideoStream.getVideoTracks()[0].enabled;
  if (enabled) {
    myVideoStream.getVideoTracks()[0].enabled = false;
    html = `<i class="fas fa-video-slash"></i>`;
    stopVideo.classList.toggle("background__red");
    stopVideo.innerHTML = html;
  } else {
    myVideoStream.getVideoTracks()[0].enabled = true;
    html = `<i class="fas fa-video"></i>`;
    stopVideo.classList.toggle("background__red");
    stopVideo.innerHTML = html;
  }
});

inviteButton.addEventListener("click", (e) => {
  prompt(
    "Copy this link and send it to people you want to meet with",
    window.location.href
  );
});

socket.on("createMessage", (message, userName) => {
  messages.innerHTML =
    messages.innerHTML +
    `<div class="message">
        <b><i class="far fa-user-circle"></i> <span> ${
          userName === USER_NAME ? "me" : userName
        }</span> </b>
        <span>${message}</span>
    </div>`;
});
