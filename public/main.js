const mainApp = document.getElementById("main__app");
const mainForm = document.getElementById("main__form");
const mainFormJoin = document.getElementById("main__form__join");

mainApp.hidden = false;
mainForm.hidden = true;
mainFormJoin.hidden = true;

const showMainWindow = () => {
    mainApp.hidden = !mainApp.hidden;
    mainForm.hidden = !mainForm.hidden;
}

const showJoinWindow = () => {
    mainApp.hidden = !mainApp.hidden;
    mainFormJoin.hidden = !mainFormJoin.hidden;
}
