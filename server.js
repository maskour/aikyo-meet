const express = require("express");
const app = express();
const server = require("http").Server(app);
const { v4: uuidv4 } = require("uuid");
app.set("view engine", "ejs");
const io = require("socket.io")(server, {
  cors: {
    origin: '*'
  }
});
const { ExpressPeerServer } = require("peer");
const path = require("path");
const url = require("url");
const peerServer = ExpressPeerServer(server, {
  debug: true,
});

app.use("/peerjs", peerServer);
app.use(express.static("public"));

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "index.html"));
});

app.get("/join", (req, res) => {
    res.redirect(
        url.format({
            pathname: `/${uuidv4()}`,
            query: req.query,
        })
    );
});

app.get("/join_room", (req, res) => {
    const { meeting_id } = req.query;
    res.redirect(
        url.format({
            pathname: `/join/${meeting_id}`,
            query: 'tedt',
        })
    )
})

app.get("/join/:room", (req, res) => {
  res.render("room", {
      roomId: req.params.room,
      username: req.query.name,
  });
});

io.on("connection", (socket) => {
  socket.on("join-room", (roomId, userId, userName) => {
    socket.join(roomId);
    socket.to(roomId).broadcast.emit("user-connected", userId, userName);

    socket.on("send-message", (message) => {
      io.to(roomId).emit("createMessage", message, userName);
    });

    socket.on('give-name', (name) => {
        socket.to(roomId).broadcast.emit('giveName', name);
    });

    socket.on('disconnect', () => {
        socket.to(roomId).broadcast.emit('user-disconnected', userId);
    });
  });
});

server.listen(process.env.PORT || 3000, () => {
    console.log("Server is running on port 3000");
});
